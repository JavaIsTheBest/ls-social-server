const aws = require("aws-sdk");
const multerS3 = require("multer-s3");
const multer = require("multer");
const path = require("path");

aws.config.update({
  secretAccessKey: process.env.AWS_S3_SECRET_ACCESS_KEY,
  accessKeyId: process.env.AWS_S3_ACCESS_KEY_ID,
  region: process.env.AWS_S3_REGION,
});

const s3 = new aws.S3();

const upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: process.env.AWS_S3_BUCKET_NAME,
    acl: "public-read",
    key: function (req, file, cb) {
      const ext = path.extname(file.originalname);
      const fileName = file.originalname.substr(
        0,
        file.originalname.lastIndexOf(ext)
      );
      req.fileName = `${fileName}-${Date.now()}${ext}`;
      cb(null, req.fileName); //use Date.now() for unique file keys
    },
  }),
});
const getAwsImageUrlFromBucket = ({ bucket, fileName }) => `https://${bucket}.s3.amazonaws.com/${fileName}`;

module.exports = {upload, getAwsImageUrlFromBucket, s3};