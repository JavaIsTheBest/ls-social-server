require('dotenv').config();

const cors = require("cors");
const express = require('express');
const db = require('./config/database');
const bodyParser = require("body-parser");
const app = express();

function addCorsMiddleware(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Headers', 'Access-Control-Allow-Origin, Content-Type, Accept, Accept-Language, Origin, User-Agent');
    if(req.method === 'OPTIONS') {
        res.sendStatus(204);
    }
    else {
        next();
    }
}

// call the database connectivity function
db();

app.use(express.json());

// app.use(myCors);
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));

//Routes
const postsRouter = require("./routes/posts");
const usersRouter = require("./routes/users");

app.use("/posts", postsRouter);
app.use("/users", usersRouter);

app.get("/posts", (req, res) => {
    res.sendFile(__dirname + "/index.html");
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, (req, res) => {
    console.log(`Server is running on ${PORT} port.`);
});