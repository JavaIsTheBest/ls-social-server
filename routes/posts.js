const express = require("express");
const router = express.Router();
const Post = require("../models/post.model");
const AWS_CONFIG = require("../config/aws-config");

Date.prototype.yyyymmdd = function() {
  let mm = this.getMonth() + 1;
  let dd = this.getDate();

  return [(dd>9 ? '' : '0') + dd,
          (mm>9 ? '' : '0') + mm,
          this.getFullYear()
         ].join('/');
};

//CRUD file for Posts

router.get("/limit/:number", async(req, res) => {
  try {
    const posts = await Post.find().limit(parseInt(req.params.number));
    res.json(posts);
  } catch (error) {
    res.send("Error: " + error);
  }
});

router.get("/", async (req, res) => {
  try {
    const posts = await Post.find();
    res.json(posts);
  } catch (error) {
    res.send("Error: " + error);
  }
});

router.get("/:uuid", async (req, res) => {
  try {
    const post = await Post.findById(req.params.uuid);
    res.json(post);
  } catch (error) {
    res.send("Error: " + error);
  }
});

router.post("/create", AWS_CONFIG.upload.single("image"), async (req, res) => {
  const date = new Date();
  const imageURL = AWS_CONFIG.getAwsImageUrlFromBucket({bucket: process.env.AWS_S3_BUCKET_NAME, fileName: req.fileName});

  const post = new Post({
    userUUID: req.body.userUUID,
    caption: req.body.caption,
    description: req.body.description,
    creationDate: date.yyyymmdd(),
    likes: 0,
    whoLiked: [],
    imageUrl: imageURL,
  });

  
  try {
    const createdPost = await post.save();
    res.json(createdPost);
  } catch (error) {
    res.send(error);
  }
});

router.patch("/update/:uuid", async (req, res) => {
  try {
    const updatedPost = await Post.findById(req.params.uuid);
    if (req.body.caption) {
      updatedPost.caption = req.body.caption;
    }
    if (req.body.description) {
      updatedPost.description = req.body.description;
    }
    if(parseInt(req.body.likes) >= 0) {
      updatedPost.likes = req.body.likes;
    }
    if(req.body.whoLiked) {
      updatedPost.whoLiked = req.body.whoLiked;
    }
    if(req.body.updateDate) {
      updatedPost.updateDate = req.body.updateDate;
    }
    await updatedPost.save();
    res.json(updatedPost);
  } catch (err) {
    console.log(err);
  }
});

//Delete
router.delete("/delete/:uuid", async (req, res) => {
  const post = await Post.findById(req.params.uuid);
  const imageName = post.imageUrl.split("/")[3];
  let params = {
    Bucket: process.env.AWS_S3_BUCKET_NAME,
    Key: imageName,
  };

  try {
    const deletedPost = await Post.remove({ _id: req.params.uuid });
    AWS_CONFIG.s3.deleteObject(params, (err, data) => {});
    res.json({});
  } catch (err) {
    console.log(err);
  }
});

module.exports = router;
