const express = require("express");
const router = express.Router();
const User = require("../models/user.model");

//CRUD file for Users

router.get("/", async(req, res) => {
    try {
        const users = await User.find();
        res.json(users);
    } catch(error) {
        res.send(error);
    }
});

router.get("/:uuid", async(req, res) => {
    try {
        const requestedUUID = req.params.uuid;
        const user = await User.findById(requestedUUID);
        res.json(post);
    } catch(error) {
        res.send(error);
    }
});

router.post("/create", async(req, res) => {
    const user = new User({
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    });

    const searchUserResults = await User.find({email: user.email});

    if(searchUserResults) {
        res.status(409);
        res.send("Email already in use!");
    } else {
        try {
            const createdUser = await user.save();
            res.json(createdUser);
        } catch(error) {
            res.send(error);
        }
    }
});

router.patch("/update/:uuid", async(req, res) => {
    try {   
        const searchUser = await User.findById(req.params.uuid);
        if(req.body.email) {
            searchUser.email = req.body.email;
        }
        if(req.body.username) {
            searchUser.username = req.body.username;
        }
        if(req.body.password) {
            searchUser.password = req.body.password;
        }
        const updatedUser = await searchUser.save();
        res.json(updatedUser);
    } catch(error) {
        res.send(error);
    }
});

router.delete("/delete/:uuid", async(req, res) => {
    try {
        await User.remove({_id: req.params.uuid});
        res.send("User has been deleted!");
    } catch(error) {
        res.send(error);
    }
});

module.exports = router;