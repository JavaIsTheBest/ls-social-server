const mongoose = require("mongoose");

// Mongo creates a uuid for each post schema so we dont need to add here postuuid
const postSchema = new mongoose.Schema({

    userUUID: {
        type: String,
        require: true
    },
    caption: {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: true
    },
    imageUrl: {
        type: String,
        require: true
    },
    creationDate: {
        type: String, 
        require: true
    },
    updateDate: {
        type: String,
        require: false
    }, 
    likes: {
        type: Number,
        require: true
    },
    whoLiked: {
        type: Array,
        required: true
    }
});

module.exports = mongoose.model("Post", postSchema);